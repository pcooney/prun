﻿A parallel test runner for nunit tests. Usage:

	prun.exe f={path to .nunit file} r={path to nunit-console.exe} t={optional timeout in milliseconds} v={optional verbose logging, true or false}

Prun executes different test assemblies in parallel. It takes a .nunit file, which defines a set of tests. The standard .nunit format can be 
extended so that an assembly can be marked as serial='false', so that it is not tested concurrently with other assemblies (see example below).

<NUnitProject>
  <Settings activeconfig="Debug"/>
  <Config name="Debug">
    <assembly path="..\testlibrary1\bin\debug\testlibrary1.dll" serial="false"/>
    <assembly path="..\serialtestlibrary1\bin\debug\serialtestlibrary1.dll" serial="true"/> 
    <assembly path="..\testlibrary2\bin\debug\testlibrary2.dll"/> <!-- serial="true" by default when omitted --> 
  </Config>
</NUnitProject>

In the example above, serialtestlibrary1.dll is run without any other test assemblies running concurrently, and once it has completed,
testlibrary1.dll and testlibrary2.dll are run concurrently.

Prun creates separate nunit-console processes for each assembly to be tested, to minimise the chance of interference between tests.


//TODO -- add an explanation of discovery, and add some handling of debug vs. release builds and find a way to ignore obj directory files.