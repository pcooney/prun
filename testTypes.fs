﻿module prun.types

type testAssembly = {
    Serial: bool; 
    AssemblyPath: string; 
}

type testRun = { 
    TestAssembly : testAssembly;
    NUnitFilePath : string;  
}

type testResult = {
     Run : uint32; 
     Errors: uint32; 
     Failures : uint32; 
     Completed: bool; 
     Text : string;
     Test : testRun;
}

type testConfig = { 
    NUnitConsolePath : string; 
    NUnitConfigPath : string;
    TimeoutMillis : int32; 
    Verbose: bool;
    Discover: bool;
    TestAssemblySuffix: string; 
    TestAssemblyParentDirectories: string;
}
        