﻿module main

open prun.types
open System.Collections.Concurrent
open System.Collections.Generic
open System.Diagnostics
open System.IO
open System.Text
open System.Threading.Tasks
open System.Xml

let getTestAssembliesFromXml (nunit : XmlDocument) = 
    seq { for a in nunit.SelectNodes("//NUnitProject/Config/assembly") do
            let serialNode = a.Attributes.GetNamedItem("serial")            
            let isSerial = not (serialNode = null) && not (serialNode.Value = null) && serialNode.Value.ToUpper() = "TRUE"
                
            yield { AssemblyPath = a.Attributes.GetNamedItem("path").Value; Serial = isSerial }
        }

let getTestAssembliesFromPath (nunitpath : string) = 
    if File.Exists nunitpath then
        let x = new XmlDocument()
        x.Load(nunitpath)
        getTestAssembliesFromXml x
    else
        Seq.empty<testAssembly>

let runTest (test : testRun) (config : testConfig) (results : ConcurrentBag<testResult>) =        
    let procInfo = new ProcessStartInfo(config.NUnitConsolePath, test.TestAssembly.AssemblyPath)
    procInfo.WorkingDirectory <- (new System.IO.FileInfo(test.NUnitFilePath)).DirectoryName
    procInfo.RedirectStandardOutput <- true
    procInfo.CreateNoWindow <- true
    procInfo.UseShellExecute <- false
    let p = new System.Diagnostics.Process()
    p.StartInfo <- procInfo

 
    if (File.Exists p.StartInfo.FileName) && p.Start() then
        let allOutput = new StringBuilder()
        let s = Stopwatch.StartNew()
    
        while not(p.WaitForExit 10) && int(s.ElapsedMilliseconds) < config.TimeoutMillis do
            ignore(allOutput.Append(p.StandardOutput.ReadToEnd()))

        ignore(allOutput.Append(p.StandardOutput.ReadToEnd()))
        
        let exp = new RegularExpressions.Regex("Tests run: ([0-9]+), Errors: ([0-9]+), Failures: ([0-9]+)")
        let expMatch = exp.Match (allOutput.ToString())

        if expMatch.Success then
            let result = { 
                Completed = true;
                Errors = (System.Convert.ToUInt32 (expMatch.Groups.[2].Captures.[0].Value));
                Failures = (System.Convert.ToUInt32 (expMatch.Groups.[3].Captures.[0].Value));
                Run = (System.Convert.ToUInt32 (expMatch.Groups.[1].Captures.[0].Value));
                Test = test;
                Text = allOutput.ToString();
                }

            results.Add(result)
        else
            if config.Verbose then
                printfn "No match for regex using output '%s'" (allOutput.ToString())

            let result = { 
                Completed = p.HasExited;
                Errors = 0u;
                Failures = 0u;
                Run = 0u;
                Test = test;
                Text = allOutput.ToString();
                }

            results.Add(result)
    ()    

let getTests pathToNUnitFile = 
    [
        for assembly in getTestAssembliesFromPath pathToNUnitFile do
            yield { TestAssembly = assembly; NUnitFilePath = pathToNUnitFile; }
    ]   

let waitForCompletion (tests : testRun list) (config : testConfig) (results : ConcurrentBag<testResult>) = 

    let endTime = System.DateTime.Now.AddMilliseconds (float config.TimeoutMillis)
    
    while System.DateTime.Now < endTime && results.Count < tests.Length do
        ignore(System.Threading.Thread.Sleep 200)

let runTests (tests : testRun list) (config : testConfig) (results : ConcurrentBag<testResult>) = 
    for test in tests do
        ignore (System.Threading.ThreadPool.QueueUserWorkItem(fun x -> runTest test config results))

    waitForCompletion tests config results

let getArgValue (arg : string) = 
    let sep = '='
    let indexOfSep = arg.IndexOf(sep)
    if indexOfSep >= 0 then
        arg.Substring(indexOfSep + 1, arg.Length - indexOfSep - 1)
    else
        arg

let getArgByName (argName : string) (args : string array) = 
    let casedArgName = argName.ToUpper()
    let mutable res = ""
    for arg in args do
        if arg.ToUpper().StartsWith(casedArgName) then
            res <- getArgValue arg
    res

let getArgByNameWithDefault argName args defaultValue = 
    let supplied = getArgByName argName args
    if System.String.IsNullOrEmpty supplied then
        defaultValue
    else
        supplied

let getNUnitPath args = 
    getArgByNameWithDefault "r" args "c:\\program files (x86)\\nunit 2.5.9\\bin\\net-2.0\\nunit-console.exe"

let getTimeout args = 
    let timeout = getArgByNameWithDefault "t" args "60000"
    int32 timeout

let getNUnitFilePath args = 
    getArgByName "f" args

let getVerbose args = 
    (getArgByName "v" args).ToUpper() = "TRUE"

let getDiscover args = 
    (getArgByName "d" args).ToUpper() = "TRUE"
   
let getTestAssemblySuffix args = 
    (getArgByName "s" args) 

let getTestAssemblyParentDirectories args = 
    (getArgByName "p" args) 

let printLine str =
    printfn str
    printfn "\r\n"

let printUsage () = 
    let pl = printLine
    pl "shortrun.exe"
    pl "USAGE:"
    pl "f={path to .nunit file}"
    pl "r={path to nunit-console.exe}"
    pl "v={optional verbose logging, true or false}"
    pl "t={optional timeout in milliseconds}"
    pl "d={optional discovery of all test assemblies, true of false, which are written to the nunit file given in the nunit file argument. The search begins at that directory, and includes sub-folders.}"
    pl "s={optional suffix used to identify test assemblies, use in conjunction with the discovery option}"
    pl "p={optional directory names used to identify test assemblies, use in conjunction with the discovery option, e.g. bin\Debug}"

let getConfig args = 
    let nunitPath = getNUnitPath args
    let nunitFilePath = getNUnitFilePath args
    let timeout = getTimeout args
    let verbose = getVerbose args
    let discover = getDiscover args
    let testAssemblySuffix = getTestAssemblySuffix args
    let testAssemblyParentDirectories = getTestAssemblyParentDirectories args
    { NUnitConsolePath = nunitPath; NUnitConfigPath = nunitFilePath; TimeoutMillis = timeout; Verbose = verbose; Discover = discover; TestAssemblySuffix = testAssemblySuffix; TestAssemblyParentDirectories = testAssemblyParentDirectories }

let printcol col fmt = 
    Printf.kprintf
        (fun s -> 
            let orig = System.Console.ForegroundColor
            System.Console.ForegroundColor <- col
            System.Console.WriteLine s
            System.Console.ForegroundColor <- orig)
        fmt

let getColour (test : testResult) = 
    let notPassed = test.Failures + test.Errors
    if notPassed > 0u then
        System.ConsoleColor.Red
    elif test.Run = 0u then
        System.ConsoleColor.Yellow
    else
        System.ConsoleColor.White

let printTest (result : testResult) = 
    printcol (getColour result) "%s\r\nRun: %d\tErrors: %d\tFailures: %d\r\n" result.Test.TestAssembly.AssemblyPath result.Run result.Errors result.Failures

let printTests results config = 

    if config.Verbose then
        Seq.iter (fun x -> (
                                System.Console.WriteLine()
                                printcol System.ConsoleColor.Blue "%s" x.Test.TestAssembly.AssemblyPath
                                System.Console.WriteLine x.Text
            )) results

    printfn "Results from %d assemblies" <| Seq.length results

    Seq.iter printTest results
 
let createParallelSets (tests : seq<testRun>) = 
    seq {
    //yield all the serial tests as single item lists
    for t in (Seq.filter  (function (x : testRun) -> x.TestAssembly.Serial) tests) do
        yield [ t ]
    
    //and then yield all the parallel tests in one list
    yield [ 
        for t in (Seq.filter (function x -> not x.TestAssembly.Serial) tests) do
            yield t
        ]
    }

let getDirectoryForSearch (config : testConfig) = 
    let info = new System.IO.FileInfo(config.NUnitConfigPath)
    info.DirectoryName

let candidateDirectory (dirInfo : System.IO.DirectoryInfo) (config : testConfig) : bool =
    System.String.IsNullOrEmpty(config.TestAssemblyParentDirectories) ||
     dirInfo.FullName.ToUpper().EndsWith(config.TestAssemblyParentDirectories.ToUpper())

let rec getTestAssemblies directoryPath config =
    let dirInfo = new System.IO.DirectoryInfo(directoryPath)
    let isCandidate = candidateDirectory dirInfo config
    seq {

    if isCandidate then
        for file in dirInfo.EnumerateFiles config.TestAssemblySuffix do
            yield file.FullName

    for directory in dirInfo.EnumerateDirectories() do
        yield! getTestAssemblies (directory.FullName) config
    }

let getTestAssembliesNoDups directoryPath config =
    let fullPathes = (getTestAssemblies directoryPath config)
    let fileInfos = seq {
        for path in fullPathes do
            yield new FileInfo(path)
    }

    let pathesAndInfos = Seq.zip fullPathes fileInfos

    let distinct = Seq.distinctBy (fun (a : string * System.IO.FileInfo) -> snd(a).Name) pathesAndInfos

    seq {
        for pathAndInfo in distinct do
            yield fst pathAndInfo
    }

let saveTestAssemblies (config : testConfig) (assemblies : seq<string>) =
    
    let fs = System.IO.File.Open(config.NUnitConfigPath, System.IO.FileMode.Create)
    let xw = new System.Xml.XmlTextWriter(fs, System.Text.Encoding.UTF8)
    
    xw.WriteStartElement "NUnitProject"
    xw.WriteStartElement "Config"
    xw.WriteAttributeString("name", "foo")
    for assembly in assemblies do
        xw.WriteStartElement "assembly"
        xw.WriteAttributeString("path", assembly)
        xw.WriteAttributeString("serial", "true")
        xw.WriteEndElement()
    xw.WriteEndElement()
    xw.WriteEndElement()
    xw.Flush()
    xw.Close()
    ()

let discoverTestAssemblies (config : testConfig) =
    if config.Discover then
        let directory = getDirectoryForSearch(config)
        if System.IO.Directory.Exists(directory) then
            ignore(saveTestAssemblies config  (getTestAssembliesNoDups directory config))
    ()

[<EntryPoint>]
let main args = 
    printUsage()

    let s = Stopwatch.StartNew()

    let config = getConfig args

    printfn "Using nunit path: %s" config.NUnitConsolePath
    printfn "Using timeout millis: %d" config.TimeoutMillis
    printfn "Using verbose: %b" config.Verbose
    printfn "Using discover: %b" config.Discover
    if config.Discover then
        printfn "Test assembly suffix: %s" config.TestAssemblySuffix
        printfn "Test assembly parent directory: %s" config.TestAssemblyParentDirectories
        discoverTestAssemblies config


    let tests = getTests (config.NUnitConfigPath)

    printcol System.ConsoleColor.Blue "Running tests in %d assemblies." tests.Length
    

    if System.IO.File.Exists config.NUnitConsolePath then
        let allResults = new List<testResult>()       
        for testSet in createParallelSets tests do
            let results = new ConcurrentBag<testResult>()        
            runTests testSet config results
            Seq.iter (fun result -> allResults.Add(result)) results

        s.Stop()
        printcol System.ConsoleColor.Blue "Completed in %d milliseconds" s.ElapsedMilliseconds
        printTests allResults config 
    else
            printcol System.ConsoleColor.Red "Path to nunit console '%s' does not exist. No tests run" config.NUnitConsolePath

    0

